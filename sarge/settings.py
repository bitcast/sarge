###########################################################################
# Sarge is Copyright (C) 2021 Kyle Robbertze <kyle@bitcast.co.za>
#
# Sarge is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3, or
# any later version as published by the Free Software Foundation.
#
# Sarge is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sarge. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
SETTINGS = {
    'instants': {
        'columns': 2,
        'files': [
            '~/music/jingles/Station ID III (2017).mp3',
            '~/music/jingles/This is UCT Radio (Voice Only).mp3',
            '~/music/jingles/Transition Effect.mp3',
            '~/music/jingles/Turn It Up ( The Soundtrack to Your Campus life).mp3',
        ],
    },
    'music_directory': '~/music/library',
    'player': {
        'sample_rate': 48000,
        'channels': 2,
    },
}
